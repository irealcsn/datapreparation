# datapreparation

## Set de datos

![Main](./imgs/main.png)

---

### P1 - ¿Cuáles son las 3 películas más populares? Es decir, las que más calificaciones han recibido, sin importar la calificación otorgada

![Pregunta1](./imgs/p1.png)

### P2 - ¿Cuáles son las 10 películas mejor calificadas por los usuarios?

![Tabla Pregunta2](./imgs/p2Table.png) ![Pregunta2](./imgs/p2.png)

### P3 - ¿Cuántas películas hay de cada género?. Si una película pertenece a más de un género, puede contarse más de una vez

![Pregunta3](./imgs/p3.png)

### P4 - ¿Qué género de películas tiene las calificaciones más altas?

 ![Pregunta4](./imgs/p4.png)

### P5 - ¿Cuál es la edad media de todos los usuarios que calificaron las películas?

 ![Pregunta5](./imgs/p5.png)

### P6 - ¿Existe alguna relación entre la edad del usuario y la calificación que otorgó a la película?

![Tabla1 Pregunta6](./imgs/p6Table2.png) 

![Tabla Pregunta6](./imgs/p6Table.png) ![Pregunta6](./imgs/p6.png)

### P7 - ¿Cuáles son las películas más populares entre los usuarios mayores de 50 años?

 ![Pregunta7](./imgs/P7.png)

### P8 - ¿Hay algún género de película que sea más popular entre los usuarios mujeres que entre los usuarios hombres?

![Pregunta8M](./imgs/p8M.png)

![Pregunta8F](./imgs/p8F.png)

### P9 - ¿Cuál es la película mejor calificada durante fines de semana, en horario de 8 a 10 P.M?

![Tabla Pregunta9](./imgs/p9Table.png) ![Pregunta9](./imgs/p9.png)